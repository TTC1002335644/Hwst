var strMap = {
	addChild : `<div class="group-row"><span class="glyphicon glyphicon-play group-icon" data-statue="off"></span><input type="text" class="form-control input-text menu-name" placeholder="菜单名"><input type="text" class="form-control input-text menu-url" placeholder="跳转链接"><button class="btn btn-operate btn-success btn-add-child">添加子菜单</button><button class="btn btn-operate btn-info btn-add">添加</button><button class="btn btn-operate btn-danger btn-del">删除</button></div>`,
	add : `<div class="group"><div class="group-row"><span class="glyphicon glyphicon-play group-icon" data-statue="off"></span><input type="text" class="form-control input-text menu-name" placeholder="菜单名"><input type="text" class="form-control input-text menu-url" placeholder="跳转链接"><button class="btn btn-operate btn-success btn-add-child">添加子菜单</button><button class="btn btn-operate btn-info btn-add">添加</button><button class="btn btn-operate btn-danger btn-del">删除</button></div></div>`,
	
};

/* 展示事件 */
$(document).on('click' , '.group-icon' ,function(){
	let status = $(this).attr('data-status');
	if(status == 'off'){
		$(this).css({
			'transform' : 'rotate(90deg)'
		}).attr('data-status','on');
		$(this).parent().siblings('.group').finish().show('fast','linear');
	}else{
		$(this).css({
			'transform' : 'rotate(0deg)'
		}).attr('data-status','off');
		$(this).parent().siblings('.group').finish().hide('fast','linear');
	}
});

/**添加子菜单**/
$(document).on('click' , '.btn-add-child' ,function(){
	let begin = `<div class="group">`;
	let end = `</div>`;
	$(this).parent().after(begin + strMap.addChild + end);
	spanTransform($(this).siblings('.group-icon') , 'on');
});

/***添加同级菜单*/
$(document).on('click' , '.btn-add' ,function(){
	$(this).parent().parent().after(strMap.add);
	
});

/**删除**/
$(document).on('click' , '.btn-del' ,function(){
	$(this).parent().parent().remove();
	if($('#form-menu').children('.group').length <= 0){
		$('#form-menu').append(strMap.add);
	}
	
});


/**旋转icon**/
function spanTransform(obj , status){
	if(status == 'on'){
		obj.css({
			'transform' : 'rotate(90deg)'
		});
	}else{
		obj.css({
			'transform' : 'rotate(0deg)'
		});
	}
}


function initMenu(){
	
}







