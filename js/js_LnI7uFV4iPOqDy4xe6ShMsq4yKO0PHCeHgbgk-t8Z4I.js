Drupal.locale = { 'strings': {"":{"Downloads":"\u4e0b\u8f7d"}} };;



Drupal.emitTrackingEvent = function() {
  var data;

  if (window.useGTM !== undefined) {
    data = {
      event: 'rz-event'
    };
    if (arguments.length > 0) {
      data.category = arguments[0];
    }
    if (arguments.length > 1) {
      data.action = arguments[1];
    }
    if (arguments.length > 2) {
      data.label = arguments[2];
    }
    if (arguments.length > 3) {
      data.value = arguments[3];
    }

    window.dataLayer.push(data);
  } else if (window.ga !== undefined) {
    data = Array.prototype.slice.call(arguments);
    data.unshift('send', 'event');
    window.ga.apply(window, data);
  }
};
