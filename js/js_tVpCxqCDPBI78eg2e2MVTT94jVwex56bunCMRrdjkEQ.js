(function ($) {

  window.StoryOverlay = {
    open: function(url) {
      var overlay = $('<div />').addClass('story-overlay').appendTo('body');
      var overlay_inner = $('<div />').addClass('story-overlay__container').appendTo(overlay);
      var overlay_container = $('<div />').addClass('story-overlay__container-inner').appendTo(overlay_inner);
      var iframe = $('<iframe />').addClass('story-overlay__iframe').appendTo(overlay_container);
      iframe.attr('src', url + '?overlay=true');

      overlay.addClass('story-overlay--active');
      $('body').addClass('body--story-overlay-active');
    },
    isOpen: function() {
      return $('body').hasClass('body--story-overlay-active');
    },
    close: function() {
      $('.story-overlay').removeClass('story-overlay--active');
      setTimeout(function() {
        $('.story-overlay').remove();
        $('body').removeClass('body--story-overlay-active');
      }, 300);
    },
    isClosed: function() {
      return !$('body').hasClass('body--story-overlay-active');
    },
    changeIframe: function(url) {
      $("iframe.story-overlay__iframe").attr("src", url + '?overlay=true')
    }
  };

  Drupal.behaviors.story = {
    attach: function(context) {
      $('.story-overlay-link', context).once('story-overlay-link').click(function(event) {
        event.preventDefault();  
        StoryOverlay.open($(this).attr('href'));
      });

      $('.story-overlay-pager', context).once('story-overlay-pager').click(function(event) {
        event.preventDefault();  
        window.parent.StoryOverlay.changeIframe($(this).attr('href'));
      });

      $('.story-overlay-close').once('story-overlay-close').click(function(event) {
          window.parent.StoryOverlay.close();
          window.parent.HashRouter.reset();
      });

      $(document).bind('keydown',function(event){
        if ( event.which == 27 ) {
           window.parent.StoryOverlay.close();
           window.parent.HashRouter.reset();
        }
      });

      var handleStoryHashChange = function(e) {
        var storyUrl = window.HashRouter.getValue('story');

        if (storyUrl) {
          window.StoryOverlay.close();

          setTimeout(function() {
            window.StoryOverlay.open(storyUrl);
          }, 3images/16);
        } else {
          window.StoryOverlay.close();
        }
      };

      $(document).bind('ready', handleStoryHashChange);
      $(window).bind('hashchange', handleStoryHashChange);
    }
  };

  // Drupal.behaviors.storyClose = {
  //   attach: function(context) {
  //     $('.story-overlay__inner', context).once('story-overlay__inner', function() {
  //       $(this).click(function(event) {
  //         if(!$(event.target).closest('.page-content__intro-block').length &&
  //            !$(event.target).closest('.story-intro__text').length &&
  //            !$(event.target).closest('.story-intro__media').length &&
  //            !$(event.target).closest('.story-block__text').length) {
  //           window.parent.StoryOverlay.close();
  //           window.parent.HashRouter.reset();
  //         }
  //       });
  //     });
  //   }
  // };

})(jQuery);
;
(function (Drupal, $) {
  Drupal.behaviors.rzAutocomplete = {
    attach: function (context) {
      $(document).ready(function(){
      $('.rz-search', context).each(function() {
        var $search = $(this);
        $('.search__button', $search).on('click', function() {
          $search.toggleClass('search--open');

          $('.search__control .form-text').focus();
        })
      })
    });
      $('.rz-search-form', context).once('rzAutocomplete').each(function() {
        var $form = $(this);
        var $s = $('input[name="keyword"]', $form);
        var $submit = $('.form-submit', $form);

        var $container = $('#js--search-suggestions');
        var $suggestions = $('<ul/>').addClass('search-suggestions__list');

        var last_search = false;

        $s.attr('autocomplete', 'off');

        $s.on('keyup', function(e) {
          var search = $(this).val();
          last_search = search;

          if (search.length < 3) {
            $suggestions.empty();
            return;
          }

          $.ajax({
            url: "/search-query/autocomplete?suggestion=" + encodeURIComponent(search),
            success: function(res) {
              if (search != last_search) {
                return;
              }
              var result = Object.keys(res).map(function(key) {
                return [res[key]]
              });
          
              if (result.length > 0) {
                $suggestions.empty();
                $.each(result, function(index, value) {
                  var $a = $('<a/>').text(value).addClass('search-suggestion__link').attr('href', '#');
                  var $li = $('<li/>').addClass('search-suggestion').append($a);
                  $suggestions.append($li);
                });
                $container.append($suggestions);
              } else {
                $suggestions.empty();
              }
            }
          });
        });

        $container.on('click', '.search-suggestion__link' , function(e) {
          e.preventDefault();
          var suggestion = $(this).text();
          $s.val(suggestion);
          $suggestions.remove();
          $form.submit()
        });

        // @REMOVED: This caused any click on the page when there are no suggestions to be intercepted
        // causing a non-functioning site.
        // $(document).on('click', function(e) {
          // if( $('#js--search-suggestions').length === 0 ) { return; }

          // if($( e.target ).closest('.rz-search-form').length === 0) {
          //   e.stopPropagation();
          //   e.preventDefault();

          //   $suggestions.remove();
          //   // $container.remove();
          // }
        // });

      });
    }
  };
})(Drupal, jQuery);
;
function shareTo(stype){
  var ftit = '';
  var flink = '';
  var lk = '';

  ftit = jQuery('.pctitle').text();

  flink = jQuery('.pcdetails img').eq(0).attr('src');
  if(typeof flink == 'undefined'){
  flink='';
  }

  if(flink == ''){
  lk = 'http://'+window.location.host+'/static/images/logo.png';
  }

  if(flink.indexOf('/uploads/') != -1) {
  lk = 'http://'+window.location.host+flink;
  }

  if(flink.indexOf('ueditor') != -1){
  lk = flink;
  }

  if(stype=='qzone'){
  window.open('https://sns.qzone.qq.com/cgi-bin/qzshare/cgi_qzshare_onekey?url='+document.location.href+'?sharesource=qzone&title='+ftit+'&pics='+lk+'&summary=瑞克斯旺中国');
  }

  if(stype=='sina'){
  window.open('http://service.weibo.com/share/share.php?url='+document.location.href+'?sharesource=weibo&title='+ftit+'&pic='+lk+'&appkey=2706825840');
  }

  if(stype == 'qq'){
    window.open('http://connect.qq.com/widget/shareqq/index.html?url='+document.location.href+'?sharesource=qzone&title='+ftit+'&pics='+lk+'&summary=瑞克斯旺中国&desc=php自学网，一个web开发交流的网站');
  }
  if(stype == 'wechat'){
    window.open('http://zixuephp.net/inc/qrcode_img.php?url=http://zixuephp.net/article-1.html');
  }
}
;
