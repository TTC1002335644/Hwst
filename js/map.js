$(document).on('click' , '.area-lists li' , function(){
	$('.area-lists li').removeClass('area-on')
	$(this).addClass('area-on')
	$('.area-lists li i').removeClass('icon-active')
	$(this).children('i').addClass('icon-active')
	let liIndex = $(this).index()
	$('.area-country-lists.country-on').removeClass('country-on')
	$('.area-country-lists').eq(liIndex).addClass('country-on')
	
})

$(document).on('click' , '.area-country-lists li' ,function(){
	$('.area-country-lists li').removeClass('country-li-on')
	$(this).addClass('country-li-on')
	
	let index = $('.area-lists li').index($('.area-on'))
	let secondIndex = $(this).index()
	
	var contactRes = createContactStr(index , secondIndex)
	initContact(contactRes)
	
})



function initContact(contactRes){
	$('.contact-info ul').empty().append(contactRes)
}




function createContactStr(index = 0 , secondIndex = 0){
	let data = mapData[index][secondIndex]
	var res = '';
	if(Array.isArray(data)){
		data.forEach(function(v){
			res = res + getStr(v.contact , v.tel , v.email , v.wechat)
		})
	}else{
		res = getStr(data.contact , data.tel , data.email , data.wechat)
	}
	return res
}


function getStr(contact = '' , tel = '' , eamil = '' , wechat = ''){
	let str = `<li>
					<p>联系人：${contact}</p>
					<p>电话：${tel}</p>
					<p>邮箱：${eamil}</p>
					<p>微信：${wechat}</p>
				</li>`
				
	return str			
}

var mapData = [
	[
		[
			{
				contact : '美国和加拿大联系人',
				tel : '美国和加拿大电话',
				email : '美国和加拿大邮箱',
				wechat : '美国和加拿大微信',
			},
			{
				contact : '美国和加拿大联系人',
				tel : '美国和加拿大电话',
				email : '美国和加拿大邮箱',
				wechat : '美国和加拿大微信',
			}
		],
		[
			{
				contact : '中美洲联系人',
				tel : '中美洲电话',
				email : '中美洲邮箱',
				wechat : '中美洲微信',
			},
			{
				contact : '中美洲联系人',
				tel : '中美洲电话',
				email : '中美洲邮箱',
				wechat : '中美洲微信',
			}
		],
		[
			{
				contact : '墨西哥联系人',
				tel : '墨西哥电话',
				email : '墨西哥邮箱',
				wechat : '墨西哥微信',
			},
			{
				contact : '墨西哥联系人',
				tel : '墨西哥电话',
				email : '墨西哥邮箱',
				wechat : '墨西哥微信',
			}
		]
		
		
		
		
	],
	[
		[
			{
				contact : '阿根廷联系人',
				tel : '阿根廷电话',
				email : '阿根廷邮箱',
				wechat : '阿根廷微信',
			},
			{
				contact : '阿根廷联系人',
				tel : '阿根廷电话',
				email : '阿根廷邮箱',
				wechat : '阿根廷微信',
			}
		],
		[
			{
				contact : '玻利维亚联系人',
				tel : '玻利维亚电话',
				email : '玻利维亚邮箱',
				wechat : '玻利维亚微信',
			},
			{
				contact : '玻利维亚联系人',
				tel : '玻利维亚电话',
				email : '玻利维亚邮箱',
				wechat : '玻利维亚微信',
			}
		],
		[
			{
				contact : '巴西联系人',
				tel : '巴西电话',
				email : '巴西邮箱',
				wechat : '巴西微信',
			},
			{
				contact : '巴西联系人',
				tel : '巴西电话',
				email : '巴西邮箱',
				wechat : '巴西微信',
			}
		],
		[
			{
				contact : '智利联系人',
				tel : '智利电话',
				email : '智利邮箱',
				wechat : '智利微信',
			},
			{
				contact : '智利联系人',
				tel : '智利电话',
				email : '智利邮箱',
				wechat : '智利微信',
			}
		],
		[
			{
				contact : '哥轮比亚联系人',
				tel : '哥轮比亚电话',
				email : '哥轮比亚邮箱',
				wechat : '哥轮比亚微信',
			},
			{
				contact : '哥轮比亚联系人',
				tel : '哥轮比亚电话',
				email : '哥轮比亚邮箱',
				wechat : '哥轮比亚微信',
			}
		],
		[
			{
				contact : '厄瓜多尔联系人',
				tel : '厄瓜多尔电话',
				email : '厄瓜多尔邮箱',
				wechat : '厄瓜多尔微信',
			},
			{
				contact : '厄瓜多尔联系人',
				tel : '厄瓜多尔电话',
				email : '厄瓜多尔邮箱',
				wechat : '厄瓜多尔微信',
			}
		],
		[
			{
				contact : '巴拉圭联系人',
				tel : '巴拉圭电话',
				email : '巴拉圭邮箱',
				wechat : '巴拉圭微信',
			},
			{
				contact : '巴拉圭联系人',
				tel : '巴拉圭电话',
				email : '巴拉圭邮箱',
				wechat : '巴拉圭微信',
			}
		],
		[
			{
				contact : '秘鲁联系人',
				tel : '秘鲁电话',
				email : '秘鲁邮箱',
				wechat : '秘鲁微信',
			},
			{
				contact : '秘鲁联系人',
				tel : '秘鲁电话',
				email : '秘鲁邮箱',
				wechat : '秘鲁微信',
			}
		],
		[
			{
				contact : '乌拉圭联系人',
				tel : '乌拉圭电话',
				email : '乌拉圭邮箱',
				wechat : '乌拉圭微信',
			},
			{
				contact : '乌拉圭联系人',
				tel : '乌拉圭电话',
				email : '乌拉圭邮箱',
				wechat : '乌拉圭微信',
			}
		],
		[
			{
				contact : '委内瑞拉联系人',
				tel : '委内瑞拉电话',
				email : '委内瑞拉邮箱',
				wechat : '委内瑞拉微信',
			},
			{
				contact : '委内瑞拉联系人',
				tel : '委内瑞拉电话',
				email : '委内瑞拉邮箱',
				wechat : '委内瑞拉微信',
			}
		]
		
	],
	[
		[
			{
				contact : '法国联系人',
				tel : '法国电话',
				email : '法国邮箱',
				wechat : '法国微信',
			},
			{
				contact : '法国联系人',
				tel : '法国电话',
				email : '法国邮箱',
				wechat : '法国微信',
			}
		],
		[
			{
				contact : '意大利联系人',
				tel : '意大利电话',
				email : '意大利邮箱',
				wechat : '意大利微信',
			},
			{
				contact : '意大利联系人',
				tel : '意大利电话',
				email : '意大利邮箱',
				wechat : '意大利微信',
			}
		],
		[
			{
				contact : '北欧联系人',
				tel : '北欧电话',
				email : '北欧邮箱',
				wechat : '北欧微信',
			},
			{
				contact : '北欧联系人',
				tel : '北欧电话',
				email : '北欧邮箱',
				wechat : '北欧微信',
			}
		],
		[
			{
				contact : '波兰联系人',
				tel : '波兰电话',
				email : '波兰邮箱',
				wechat : '波兰微信',
			},
			{
				contact : '波兰联系人',
				tel : '波兰电话',
				email : '波兰邮箱',
				wechat : '波兰微信',
			}
		],
		[
			{
				contact : '俄国联系人',
				tel : '俄国电话',
				email : '俄国邮箱',
				wechat : '俄国微信',
			},
			{
				contact : '俄国联系人',
				tel : '俄国电话',
				email : '俄国邮箱',
				wechat : '俄国微信',
			}
		],
		[
			{
				contact : '东南欧联系人',
				tel : '东南欧电话',
				email : '东南欧邮箱',
				wechat : '东南欧微信',
			},
			{
				contact : '东南欧联系人',
				tel : '东南欧电话',
				email : '东南欧邮箱',
				wechat : '东南欧微信',
			}
		],
		[
			{
				contact : '西班牙和葡萄牙联系人',
				tel : '西班牙和葡萄牙电话',
				email : '西班牙和葡萄牙邮箱',
				wechat : '西班牙和葡萄牙微信',
			},
			{
				contact : '西班牙和葡萄牙联系人',
				tel : '西班牙和葡萄牙电话',
				email : '西班牙和葡萄牙邮箱',
				wechat : '西班牙和葡萄牙微信',
			}
		],
		[
			{
				contact : '火鸡联系人',
				tel : '火鸡电话',
				email : '火鸡邮箱',
				wechat : '火鸡微信',
			},
			{
				contact : '火鸡联系人',
				tel : '火鸡电话',
				email : '火鸡邮箱',
				wechat : '火鸡微信',
			}
		],
		[
			{
				contact : '乌克兰联系人',
				tel : '乌克兰电话',
				email : '乌克兰邮箱',
				wechat : '乌克兰微信',
			},
			{
				contact : '乌克兰联系人',
				tel : '乌克兰电话',
				email : '乌克兰邮箱',
				wechat : '乌克兰微信',
			}
		]
		
	],
	[
		[
			{
				contact : '澳大利亚和新西兰联系人',
				tel : '澳大利亚和新西兰电话',
				email : '澳大利亚和新西兰邮箱',
				wechat : '澳大利亚和新西兰微信',
			},
			{
				contact : '澳大利亚和新西兰联系人',
				tel : '澳大利亚和新西兰电话',
				email : '澳大利亚和新西兰邮箱',
				wechat : '澳大利亚和新西兰微信',
			}
		],
		[
			{
				contact : '中亚联系人',
				tel : '中亚电话',
				email : '中亚邮箱',
				wechat : '中亚微信',
			},
			{
				contact : '中亚联系人',
				tel : '中亚电话',
				email : '中亚邮箱',
				wechat : '中亚微信',
			}
			
		],
		[
			{
				contact : '中国联系人',
				tel : '中国电话',
				email : '中国邮箱',
				wechat : '中国微信',
			},
			{
				contact : '中国联系人',
				tel : '中国电话',
				email : '中国邮箱',
				wechat : '中国微信',
			}
			
		],
		[
			{
				contact : '印度尼西亚联系人',
				tel : '印度尼西亚电话',
				email : '印度尼西亚邮箱',
				wechat : '印度尼西亚微信',
			},
			{
				contact : '印度尼西亚联系人',
				tel : '印度尼西亚电话',
				email : '印度尼西亚邮箱',
				wechat : '印度尼西亚微信',
			}
		],
		[
			{
				contact : '日本联系人',
				tel : '日本电话',
				email : '日本邮箱',
				wechat : '日本微信',
			},
			{
				contact : '日本联系人',
				tel : '日本电话',
				email : '日本邮箱',
				wechat : '日本微信',
			}
		],
		[
			{
				contact : '印度联系人',
				tel : '印度电话',
				email : '印度邮箱',
				wechat : '印度微信',
			},
			{
				contact : '印度联系人',
				tel : '印度电话',
				email : '印度邮箱',
				wechat : '印度微信',
			}
		],
		[
			{
				contact : '东南亚联系人',
				tel : '东南亚电话',
				email : '东南亚邮箱',
				wechat : '东南亚微信',
			},
			{
				contact : '东南亚联系人',
				tel : '东南亚电话',
				email : '东南亚邮箱',
				wechat : '东南亚微信',
			}
		],
		[
			{
				contact : '泰国联系人',
				tel : '泰国电话',
				email : '泰国邮箱',
				wechat : '泰国微信',
			},
			{
				contact : '泰国联系人',
				tel : '泰国电话',
				email : '泰国邮箱',
				wechat : '泰国微信',
			}
		],
		[
			{
				contact : '越南联系人',
				tel : '越南电话',
				email : '越南邮箱',
				wechat : '越南微信',
			},
			{
				contact : '越南联系人',
				tel : '越南电话',
				email : '越南邮箱',
				wechat : '越南微信',
			}
		]
		
	],
	[
		[
			{
				contact : '以色列联系人',
				tel : '以色列电话',
				email : '以色列邮箱',
				wechat : '以色列微信',
			},
			{
				contact : '以色列联系人',
				tel : '以色列电话',
				email : '以色列邮箱',
				wechat : '以色列微信',
			}
		],
		[
			{
				contact : '中东联系人',
				tel : '中东电话',
				email : '中东邮箱',
				wechat : '中东微信',
			},
			{
				contact : '中东联系人',
				tel : '中东电话',
				email : '中东邮箱',
				wechat : '中东微信',
			}
		]
		
		
	],
	[
		[
			{
				contact : '非洲联系人',
				tel : '非洲电话',
				email : '非洲邮箱',
				wechat : '非洲微信',
			},
			{
				contact : '非洲联系人',
				tel : '非洲电话',
				email : '非洲邮箱',
				wechat : '非洲微信',
			}
			
		],
		[
			{
				contact : '也格里布联系人',
				tel : '也格里布电话',
				email : '也格里布邮箱',
				wechat : '也格里布微信',
			},
			{
				contact : '也格里布联系人',
				tel : '也格里布电话',
				email : '也格里布邮箱',
				wechat : '也格里布微信',
			}
		]
		
		
	]
	
]


initContact(createContactStr(0,0))