const productBannerLiWidth = $('.product-banner li').width();
console.log(productBannerLiWidth)

//上一个
$('.product-banner-prev').click(function(){
	getLiObject('prev')
})

//下一个
$('.product-banner-next').click(function(){
	getLiObject('next')
})

$('.product-banner li').click(function(){
	let className = 'on'
	//获取当前的index
	let currentObj = $('.product-banner li.'+className)
	changeClass(currentObj,$(this),className)
})



function getLiObject(type = ''){
	let className = 'on'
	//获取当前的index
	let currentObj = $('.product-banner li.'+className)
	
	if(type == 'prev'){
		var changeObj = currentObj.prev()
	}else if(type == 'next'){
		var changeObj = currentObj.next()
	}else{
		return;
	}
	
	if(changeObj.length == 0){
		return;
	}
	changeClass(currentObj,changeObj,className)
}

function changeClass(current  , change , className = 'on'){
	current.removeClass(className)
	change.addClass(className)
	console.log(change.children('img').attr('src'))
	$('.product-banner-show').attr('src',change.children('img').attr('src'))
}

function moveList(type = ''){
	var obj = $('.product-banner')
	var allWidth = obj.width()
	if(type == 'prev'){
		var width = productBannerLiWidth
	}else if(type == 'next'){
		var width =  -1 * productBannerLiWidth
	}else{
		return;
	}
	
	console.log(width)
	console.log(allWidth)
	
	
	
	obj.css({
		transform: "translateX(" + width + "px)",
	});
}