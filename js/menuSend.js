function getFormData(){
	var data = getChild($('#form-menu'));
	return data;
}

function getChild(obj){
	var groupObj =  obj.children('.group');
	var returnData = {};
	$.each(groupObj,function(k , v){
		let name = $(v).children('.group-row').children('.menu-name').val();
		let url = $(v).children('.group-row').children('.menu-url').val();
		if(name.length > 0){
			returnData[k] = {
				data : {
					name : name,
					url : url,
					child : getChild($(v))
				}
			};
		}
	});
	return returnData;
}


$('.btn-save').click(function(){
	var data = getFormData();
	console.log(data);
});